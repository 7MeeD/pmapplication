package com.example.pmapplication.ui.home;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.pmapplication.R;

import java.util.ArrayList;
import java.util.List;

public class homeAdapter extends BaseAdapter {
    Context context;
    private int layout;
    private List<HomeViewModel> personnelList;
    public homeAdapter(Context context,int resource, List<HomeViewModel> personnelList) {
        this.context=context;
        this.layout=resource;
        this.personnelList=personnelList;
        Log.e("Created","weird");
    }

    @Override
    public int getCount() {
        return personnelList.size();
    }

    @Override
    public Object getItem(int position) {
        return personnelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    private class ViewHolder {
        TextView nameTxt, from_date,to_date,cost;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.nameTxt = (TextView) row.findViewById(R.id.project_name);
            holder.from_date = (TextView) row.findViewById(R.id.from_date);
            holder.to_date = (TextView) row.findViewById(R.id.to_date);
            holder.cost = (TextView) row.findViewById(R.id.cost);
            Log.e("category", "First");
            HomeViewModel model = personnelList.get(position);
            Log.e("category", model.getName());
            holder.nameTxt.setText(model.getName());
            holder.from_date.setText(model.getsDat());
            holder.to_date.setText(model.geteDat());
            holder.cost.setText(model.getCost());
        return row;
    }
}

package com.example.pmapplication.ui.home;

public class task_resourceModel {
    private String ppId,tID,rID,name,cost,type;

    public task_resourceModel(String ppId,String tID,String rID,String name,String type,String cost){
        this.ppId=ppId;
        this.tID=tID;
        this.rID=rID;
        this.name=name;
        this.cost=cost;
        this.type=type;
    }

    public String getPpId() {
        return ppId;
    }

    public void setPpId(String ppId) {
        this.ppId = ppId;
    }

    public String gettID() {
        return tID;
    }

    public void settID(String tID) {
        this.tID = tID;
    }

    public String getrID() {
        return rID;
    }

    public void setrID(String rID) {
        this.rID = rID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

package com.example.pmapplication.ui.home;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

public class taskModel {
    private String tID;
    private String ppId,rID;
    private String name;
    private String sDat;
    private String eDat;
    private String duration;
    private String cost;

    public taskModel(String tID, String ppID, String rID, String tName, String sDat, String eDat, String duration,String cost){
        this.tID=tID;
        this.ppId=ppID;
        this.name=tName;
        this.sDat=sDat;
        this.eDat=eDat;
        this.rID=rID;
        this.duration=duration;
        this.cost=cost;
    }

    public String getrID() {
        return rID;
    }

    public void setrID(String rID) {
        this.rID = rID;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String gettID() {
        return tID;
    }

    public void settID(String tID) {
        this.tID = tID;
    }

    public String getPpId() {
        return ppId;
    }

    public void setPpId(String ppId) {
        this.ppId = ppId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getsDat() {
        return sDat;
    }

    public void setsDat(String sDat) {
        this.sDat = sDat;
    }

    public String geteDat() {
        return eDat;
    }

    public void seteDat(String eDat) {
        this.eDat = eDat;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}

package com.example.pmapplication.ui.home;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.example.pmapplication.DbHelper;
import com.example.pmapplication.R;
import com.example.pmapplication.project_main;
import com.example.pmapplication.ui.main.PlaceholderFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {
    private ListView lstView;
    private ArrayList<HomeViewModel> checklists;
    private HomeViewModel homeViewModel;
    private DbHelper mydb ;
    public static homeAdapter adapter;
    private String project_ID;
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mydb = new DbHelper(getContext());
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        lstView = (ListView) root.findViewById(R.id.lstView);
        checklists = new ArrayList<HomeViewModel>();
        SQLiteDatabase sqlDb = mydb.getReadableDatabase();
        Cursor res = mydb.getAllProjects();
        adapter= new homeAdapter(getActivity(),R.layout.projects_list,checklists);
        lstView.setAdapter(adapter);
        while (res.moveToNext()){
            int ID = res.getInt(0);
            project_ID=String.valueOf(ID);
            String name = res.getString(1);
            Log.e("Test ID",String.valueOf(ID));
            String from_date = res.getString(2);
            String to_date = res.getString(3);
            String cost = res.getString(4);
            checklists.add(new HomeViewModel(String.valueOf(ID),name, from_date, to_date, cost,null));
            Log.e("quantity",String.valueOf(checklists.size()));
        }

        lstView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), project_main.class);
                intent.putExtra("project_id",checklists.get(position).getpId());
                Log.e("pass project id",checklists.get(position).getpId());
                startActivity(intent);
            }
        });
        Log.e("we're","out");
//        adapter.notifyDataSetChanged();
        return root;
    }
}
package com.example.pmapplication.ui.home;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.pmapplication.R;
import com.example.pmapplication.resourceAdapter;

import java.util.List;

public class task_resourceAdapter extends BaseAdapter {
    Context context;
    private int layout;
    private List<task_resourceModel> personnelList;

    public task_resourceAdapter(Context context,int resource, List<task_resourceModel> personnelList) {
        this.context=context;
        this.layout=resource;
        this.personnelList=personnelList;
    }

    @Override
    public int getCount() {        return personnelList.size();    }

    @Override
    public Object getItem(int position) {        return personnelList.get(position);    }

    @Override
    public long getItemId(int position) { return position;    }

    private class ViewHolder {
        TextView nameTxt, from_date,to_date;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = new ViewHolder();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(layout, null);
        holder.nameTxt = (TextView) row.findViewById(R.id.orderTime);
        holder.from_date = (TextView) row.findViewById(R.id.deliveryTime);
        holder.to_date = (TextView) row.findViewById(R.id.finalprice);
        task_resourceModel model = personnelList.get(position);
        holder.nameTxt.setText(model.getName());
        holder.from_date.setText(model.getType());
        holder.to_date.setText(model.getCost());
        return row;
    }
}

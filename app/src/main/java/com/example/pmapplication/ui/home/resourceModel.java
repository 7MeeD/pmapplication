package com.example.pmapplication.ui.home;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


public class resourceModel {
    private String rID;
    private String tID;
    private String ppId;
    private String name;
    private String type;

    public resourceModel(String rID,String ppID, String Name, String type){
        this.rID=rID;
        this.tID=tID;
        this.ppId=ppID;
        this.name=Name;
        this.type=type;
    }

    public String getrID() {
        return rID;
    }

    public void setrID(String rID) {
        this.rID = rID;
    }

    public String gettID() {
        return tID;
    }

    public void settID(String tID) {
        this.tID = tID;
    }

    public String getPpId() {
        return ppId;
    }

    public void setPpId(String ppId) {
        this.ppId = ppId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

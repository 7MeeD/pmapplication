package com.example.pmapplication.ui.main;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.pmapplication.DbHelper;
import com.example.pmapplication.R;
import com.example.pmapplication.project_main;
import com.example.pmapplication.resourceAdapter;
import com.example.pmapplication.taskAdapter;
import com.example.pmapplication.ui.home.resourceModel;
import com.example.pmapplication.ui.home.taskModel;

import java.util.ArrayList;

import static com.example.pmapplication.project_main.passed_project_id;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {
    private DatePickerDialog picker;
    private DbHelper mydb;
    Cursor cSum;
    private ListView lstView;
    public static taskAdapter adapter;
    public static resourceAdapter adapter2;
    public static ArrayList<taskModel> checklists;
    public static ArrayList<resourceModel> checklists2;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private PageViewModel pageViewModel;
    int index;

    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull final LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        Log.e("tab", String.valueOf(index));
        if (index == 1) {
            mydb = new DbHelper(getContext());
            checklists = new ArrayList<taskModel>();
            adapter = new taskAdapter(getContext(), R.layout.task_list, checklists);
            lstView = (ListView) root.findViewById(R.id.lstView2);
            lstView.setAdapter(adapter);
            SQLiteDatabase sqlDb = mydb.getReadableDatabase();
            Cursor res = mydb.getAlltasks(project_main.passed_project_id);
            while (res.moveToNext()) {
                int ID = res.getInt(0);
                Log.e("Task ID is ",String.valueOf(ID));
                int pID = res.getInt(1);
                Log.e("project ID inside task", String.valueOf(pID));
                String resourceID = res.getString(2);
                Log.e("resource for the task", resourceID);
                String name = res.getString(3);
                Log.e("Task name", String.valueOf(name));
                String from_date = res.getString(4);
                Log.e("from_date", from_date);
                String to_date = res.getString(5);
                Log.e("to_date", to_date);
                String duration = res.getString(6);
                Log.e("duration is", duration);
                    String cost = res.getString(7);
                    Log.e("cost is", cost);
                checklists.add(new taskModel(String.valueOf(ID), String.valueOf(project_main.passed_project_id),String.valueOf(0),name, from_date, to_date, duration,cost));
                Log.e("quantity", String.valueOf(checklists.size()));
            }
            cSum = sqlDb.rawQuery("UPDATE projects SET cost = ("+"select sum(cost) from tasks where projectID="+passed_project_id+") where id="+passed_project_id+"" , null);
            adapter.notifyDataSetChanged();
        } else if (index == 2) {
            mydb = new DbHelper(getContext());
            checklists2 = new ArrayList<resourceModel>();
            adapter2 = new resourceAdapter(getContext(), R.layout.resource_list, checklists2);
            lstView = (ListView) root.findViewById(R.id.lstView2);
            lstView.setAdapter(adapter2);
            SQLiteDatabase sqlDb = mydb.getReadableDatabase();
            Cursor res = mydb.getAllresources(project_main.passed_project_id);
            while (res.moveToNext()) {
                int ID = res.getInt(0);
                int pID = res.getInt(1);
                String name = res.getString(2);
                Log.e("resource name is",name);
                String type = res.getString(3);
                checklists2.add(new resourceModel(String.valueOf(ID), String.valueOf(pID), name, type));
                Log.e("quantity", String.valueOf(checklists2.size()));
            }
        }
        return root;
    }
}
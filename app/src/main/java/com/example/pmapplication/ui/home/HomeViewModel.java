package com.example.pmapplication.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "projects")
public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private String pId;
    private String name;
    private String sDat;
    private String eDat;
    private String cost;
    private String place;


    public HomeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }


    public HomeViewModel(String id, String name, String sD, String ed, String cost, String place) {
        this.pId=id;
        this.name=name;
        this.sDat=sD;
        this.eDat=ed;
        this.cost=cost;
        this.place=place;
    }
    public LiveData<String> getText() {
        return mText;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getsDat() {
        return sDat;
    }

    public void setsDat(String sDat) {
        this.sDat = sDat;
    }

    public String geteDat() {
        return eDat;
    }

    public void seteDat(String eDat) {
        this.eDat = eDat;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
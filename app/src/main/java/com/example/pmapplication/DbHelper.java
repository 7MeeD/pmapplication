package com.example.pmapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

import static com.example.pmapplication.project_main.passed_project_id;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "PM.db";
    public static final String CONTACTS_TABLE_NAME = "projects";
    public static final String CONTACTS_TABLE_NAME_02 = "tasks";
    public static final String CONTACTS_TABLE_NAME_03 = "resources";
    public static final String CONTACTS_COLUMN_ID = "resourceID";
    public static final String CONTACTS_COLUMN_ID_02 = "cost";
    public static final String CONTACTS_COLUMN_ID_03 = "taskID";
    public static final String CONTACTS_COLUMN_NAME = "id";
    public static final String CONTACTS_COLUMN_FROM_DATE = "email";
    public static final String CONTACTS_COLUMN_TO_DATE = "street";
    public static final String CONTACTS_COLUMN_CITY = "place";
    public static final String CONTACTS_COLUMN_COST = "cost";
    private HashMap hp;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME , null, 12);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS projects");
        db.execSQL("DROP TABLE IF EXISTS tasks");
        db.execSQL("DROP TABLE IF EXISTS resources");
        db.execSQL("DROP TABLE IF EXISTS task_resources");
        db.execSQL(
                "create table projects " +
                        "(id integer primary key, name text,from_date text,to_date text, cost text,place text)"
        );
        db.execSQL(
                "create table tasks " +
                        "(id integer primary key,projectID integer,resourceID int,name text,from_date text,to_date text, duration text,cost text)"
        );
        db.execSQL(
                "create table resources " +
                        "(id integer primary key,projectID integer,name text,type text)"
        );
        db.execSQL(
                "create table task_resources " +
                        "(id integer primary key,projectID integer,taskID integer,resourceID integer,name text,type text,cost text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS projects");
        onCreate(db);
    }

    public boolean insertProject (String name, String from_date, String to_date, String cost,String place) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("from_date", from_date);
        contentValues.put("to_date", to_date);
        contentValues.put("cost", cost);
        contentValues.put("place", place);
        db.insert("projects", null, contentValues);
        return true;
    }

    public boolean insertTask (int projectID,int resourceID,String name, String from_date, String to_date, String duration,String cost) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("projectID", projectID);
        contentValues.put("resourceID", resourceID);
        contentValues.put("name", name);
        contentValues.put("from_date", from_date);
        contentValues.put("to_date", to_date);
        contentValues.put("duration", duration);
        contentValues.put("cost", cost);
        db.insert("tasks", null, contentValues);
        db.rawQuery("UPDATE projects SET cost = ("+"select sum(cost) from tasks where projectID="+passed_project_id+") where id="+passed_project_id+"" , null);
        return true;
    }

    public boolean insertResource (int projectID,String name,String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("projectID", projectID);
        contentValues.put("name", name);
        contentValues.put("type", type);
        db.insert("resources", null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from projects where id="+id+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }

    public Cursor getAllProjects() {
        try {
            String selectQuery = "SELECT * FROM projects";
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            return cursor;
        } catch (Exception ex) {
            return null;
        }
    }

    public Cursor getAlltasks(int id) {
        try {
            String selectQuery = "SELECT * FROM tasks where projectID="+id+"";
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            return cursor;
        } catch (Exception ex) {
            return null;
        }
    }

    public Cursor getAlltasks() {
        try {
            String selectQuery = "SELECT * FROM tasks";
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            return cursor;
        } catch (Exception ex) {
            return null;
        }
    }

    public Cursor getAllresources(int pid) {
        try {
            String selectQuery = "SELECT * FROM resources where projectID="+pid+"";
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            return cursor;
        } catch (Exception ex) {
            return null;
        }
    }

    public Cursor getAlltask_resources(int pid,int tid) {
        try {
            String selectQuery = "SELECT * FROM task_resources where projectID="+pid+" and taskID="+tid+"";
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            Log.e("name resource",String.valueOf(tid));
            return cursor;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean assignResource(int pid,int rid, int tid,String name,String type, String cost) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("projectID", pid);
        contentValues.put("taskID", tid);
        contentValues.put("resourceID", rid);
        contentValues.put("name", name);
        contentValues.put("type", type);
        contentValues.put("cost", cost);
        db.insert("task_resources", null, contentValues);
            return true;
    }
}
package com.example.pmapplication;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pmapplication.ui.home.resourceModel;
import com.example.pmapplication.ui.home.taskModel;
import com.example.pmapplication.ui.home.task_resourceAdapter;
import com.example.pmapplication.ui.home.task_resourceModel;

import java.util.ArrayList;

import static com.example.pmapplication.project_main.passed_project_id;
import static com.example.pmapplication.taskAdapter.passed_task_id;

public class task_detail extends AppCompatActivity {
    private DbHelper mydb ;
    private ListView lstView;
    private task_resourceAdapter adapter;
    private ArrayList<task_resourceModel> checklists2;
    private Cursor cSum,tSum;
    int taskID=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mydb = new DbHelper(task_detail.this);
        setContentView(R.layout.task_resource_detail);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        TextView title = (TextView) findViewById(R.id.medium_TextView);
//        String tskNM = bundle.getString("taskname");
//        title.setText(tskNM);
        checklists2 = new ArrayList<task_resourceModel>();
        Log.e("Hello","I'm in detail");
        taskID=bundle.getInt("taskid");
        adapter = new task_resourceAdapter(task_detail.this, R.layout.list_task_resource, checklists2);
        lstView = (ListView) findViewById(R.id.lstView3);
        lstView.setAdapter(adapter);
        SQLiteDatabase sqlDb = mydb.getReadableDatabase();

        Cursor res = mydb.getAlltask_resources(passed_project_id,taskID);
        while (res.moveToNext()) {
            int pID = res.getInt(1);
            int tID = res.getInt(2);
            int resourceID = res.getInt(3);
            String resourceName = res.getString(4);
            String resourceType = res.getString(5);
            String resourceCost = res.getString(6);
            Log.e("resource taskID is",String.valueOf(tID));
            Log.e("resource cost is",resourceCost);
            checklists2.add(new task_resourceModel(String.valueOf(pID), String.valueOf(tID),String.valueOf(resourceID),resourceName, resourceType, resourceCost));
        }
        Log.e("up pro when add task",String.valueOf(passed_project_id));
        Log.d("up task", String.valueOf(taskID));
        cSum = sqlDb.rawQuery("UPDATE projects SET cost = ("+"select sum(cost) from tasks where projectID="+passed_project_id+") where id="+passed_project_id+"" , null);
        if(cSum.moveToFirst()) {
            Log.e("Summation",String.valueOf(cSum.getInt(0)));
        }
    }
}

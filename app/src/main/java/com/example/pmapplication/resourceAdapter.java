package com.example.pmapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.pmapplication.ui.home.resourceModel;
import com.example.pmapplication.ui.home.taskModel;

import java.util.List;

public class resourceAdapter extends BaseAdapter {
    Context context;
    private int layout;
    private List<resourceModel> personnelList;
    public resourceAdapter(Context context,int resource, List<resourceModel> personnelList) {
        this.context=context;
        this.layout=resource;
        this.personnelList=personnelList;
        Log.e("Created","weird23");
    }

    @Override
    public int getCount() {
        return personnelList.size();
    }

    @Override
    public Object getItem(int position) {
        return personnelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    private class ViewHolder {
        TextView nameTxt, from_date,to_date,duration;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = new ViewHolder();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(layout, null);
        holder.nameTxt = (TextView) row.findViewById(R.id.project_name2);
        holder.from_date = (TextView) row.findViewById(R.id.from_date5);
        holder.to_date = (TextView) row.findViewById(R.id.to_date5);
        holder.duration = (TextView) row.findViewById(R.id.duration);
        Log.e("category", "First");
        resourceModel model = personnelList.get(position);
        Log.e("category", model.getName());
        holder.nameTxt.setText(model.getName());
        holder.from_date.setText(model.getType());
        notifyDataSetChanged();
        return row;
    }
}

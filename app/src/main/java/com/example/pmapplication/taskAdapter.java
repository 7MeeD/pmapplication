package com.example.pmapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pmapplication.ui.home.HomeFragment;
import com.example.pmapplication.ui.home.HomeViewModel;
import com.example.pmapplication.ui.home.homeAdapter;
import com.example.pmapplication.ui.home.resourceModel;
import com.example.pmapplication.ui.home.taskModel;

import java.util.ArrayList;
import java.util.List;

import static com.example.pmapplication.project_main.passed_project_id;

public class taskAdapter extends BaseAdapter {
    private DatePickerDialog picker;
    private DbHelper mydb;
    private ListView lstView;
    private taskAdapter adapter;
    private resourceAdapter adapter2;
    private ArrayList<taskModel> checklists;
    private ArrayList<resourceModel> checklists2;
    public static int passed_resource_id = 0;
    public static int passed_task_id=0;
    public static int passed_task_cost=0;

    Context context;
    private int layout;
    private List<taskModel> personnelList;
    String strName;
    public taskAdapter(Context context,int resource, List<taskModel> personnelList) {
        this.context=context;
        this.layout=resource;
        this.personnelList=personnelList;
        Log.e("Created","weird22");
    }

    @Override
    public int getCount() {
        return personnelList.size();
    }

    @Override
    public Object getItem(int position) {
        return personnelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    private class ViewHolder {
        TextView nameTxt, from_date,to_date,duration,cost;
        ImageButton assign;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = new ViewHolder();
        final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(layout, null);
        holder.nameTxt = (TextView) row.findViewById(R.id.project_name2);
        holder.from_date = (TextView) row.findViewById(R.id.from_date5);
        holder.to_date = (TextView) row.findViewById(R.id.to_date5);
        holder.duration = (TextView) row.findViewById(R.id.duration);
        holder.cost = (TextView) row.findViewById(R.id.cost2);
        holder.assign = (ImageButton) row.findViewById(R.id.assign);

        Log.e("category", "First");
        final taskModel model = personnelList.get(position);
        Log.e("category", model.getName());
        holder.nameTxt.setText(model.getName());
        holder.from_date.setText(model.getsDat());
        holder.to_date.setText(model.geteDat());
        holder.duration.setText(model.getDuration());
        holder.cost.setText(model.getCost());
        holder.assign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passed_task_id = Integer.parseInt(model.gettID());
                passed_task_cost = Integer.parseInt(model.getCost());
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                builderSingle.setTitle("Select One Name:-");
                mydb = new DbHelper(context);
                checklists2 = new ArrayList<resourceModel>();
                SQLiteDatabase sqlDb = mydb.getReadableDatabase();
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_singlechoice);
                Cursor res = mydb.getAllresources(passed_project_id);
                while (res.moveToNext()) {
                    int ID = res.getInt(0);
                    int pID = res.getInt(1);
                    String name = res.getString(2);
                    arrayAdapter.add(name);
                    Log.e("resource name is",name);
                    String type = res.getString(3);
                    checklists2.add(new resourceModel(String.valueOf(ID), String.valueOf(pID), name, type));
                    Log.e("quantity", String.valueOf(checklists2.size()));
                }
                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        passed_resource_id = Integer.parseInt(checklists2.get(which).getrID());
                        final String resource_name = checklists2.get(which).getName();
                        final String resource_type = checklists2.get(which).getType();
                        Log.e("inside second dialog",checklists2.get(which).getrID());
                        strName = arrayAdapter.getItem(which);
                        AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                        final View customLayout = inflater.inflate(R.layout.custom_layout_dialog, null);
                        builderInner.setView(customLayout);
                        builderInner.setMessage(strName);
                        builderInner.setTitle("Your Selected Item is");
                        final TextView poV = customLayout.findViewById(R.id.poV);
                        poV.setVisibility(View.GONE);
                        final EditText name = customLayout.findViewById(R.id.name2);
                        name.setVisibility(View.GONE);
                        final TextView toV = customLayout.findViewById(R.id.toV);
                        final TextView foV = customLayout.findViewById(R.id.foV);
                        toV.setVisibility(View.GONE);
                        foV.setVisibility(View.GONE);
                        final EditText from_date = customLayout.findViewById(R.id.date3);
                        final EditText to_date = customLayout.findViewById(R.id.date4);
                        from_date.setVisibility(View.GONE);
                        to_date.setVisibility(View.GONE);
                        final TextView dur = customLayout.findViewById(R.id.duview);
                        dur.setVisibility(View.GONE);
                        final EditText duration = customLayout.findViewById(R.id.duration);
                        duration.setVisibility(View.GONE);
                        final TextView cos = customLayout.findViewById(R.id.costview);
                        cos.setVisibility(View.GONE);
                        final EditText cost = customLayout.findViewById(R.id.cost);
                        cost.setVisibility(View.GONE);
                        builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                final String cost = name.getText().toString();
                                mydb.assignResource(passed_project_id,passed_resource_id,passed_task_id,resource_name,resource_type,String.valueOf(passed_task_cost));
                                SQLiteDatabase sqlDb = mydb.getReadableDatabase();
                                Cursor cSum = sqlDb.rawQuery("UPDATE projects SET cost = ("+"select sum(cost) from tasks where projectID="+passed_project_id+") where id="+passed_project_id+"" , null);
//                                Cursor tSum = sqlDb.rawQuery("UPDATE tasks SET cost = ("+"select sum(cost) from task_resources where projectID="+passed_project_id+" and taskID ="+passed_task_id+") where projectID="+passed_project_id+" and id ="+passed_task_id+"" , null);
                                HomeFragment.adapter.notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        });
                        builderInner.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builderInner.show();
                    }
                });
                builderSingle.show();
            }
        });

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, task_detail.class);
                intent.putExtra("taskid",position+1);
                intent.putExtra("taskname",strName);
                Log.e("passed adTask is",String.valueOf(position+1));
                Log.e("passed proTask is",String.valueOf(passed_project_id));
                context.startActivity(intent);
            }
        });
        notifyDataSetChanged();
        return row;
    }
}

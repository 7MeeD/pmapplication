package com.example.pmapplication;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.example.pmapplication.ui.home.HomeViewModel;
import com.example.pmapplication.ui.home.homeAdapter;
import com.example.pmapplication.ui.home.resourceModel;
import com.example.pmapplication.ui.home.taskModel;
import com.example.pmapplication.ui.main.PlaceholderFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pmapplication.ui.main.SectionsPagerAdapter;

import java.util.ArrayList;
import java.util.Calendar;

public class project_main extends AppCompatActivity{
    private DatePickerDialog picker;
    private DbHelper mydb ;
    public static int passed_project_id=0;
    String value;
    SQLiteDatabase sqlDb;
    private Cursor cSum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mydb = new DbHelper(project_main.this);
        sqlDb = mydb.getReadableDatabase();
        value= getIntent().getStringExtra("project_id");
            Log.e("here project ID",value);
            passed_project_id = Integer.parseInt(value);
        setContentView(R.layout.activity_project_main);
        final SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        final TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tabs.getSelectedTabPosition() == 0) {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(project_main.this);
                    alert.setTitle("Enter task information");
                    final View customLayout = getLayoutInflater().inflate(R.layout.custom_layout_dialog, null);
                    alert.setView(customLayout);
                    final EditText name = customLayout.findViewById(R.id.name2);
                    final EditText from_date = customLayout.findViewById(R.id.date3);
                    final EditText to_date = customLayout.findViewById(R.id.date4);
                    final EditText duration = customLayout.findViewById(R.id.duration);
                    final EditText cost = customLayout.findViewById(R.id.cost);
                    from_date.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Calendar cldr = Calendar.getInstance();
                            Log.e("test","clicked date");
                            int day = cldr.get(Calendar.DAY_OF_MONTH);
                            int month = cldr.get(Calendar.MONTH);
                            int year = cldr.get(Calendar.YEAR);
                            // date picker dialog
                            picker = new DatePickerDialog(project_main.this,
                                    new DatePickerDialog.OnDateSetListener() {
                                        @Override
                                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                            from_date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                        }
                                    }, year, month, day);
                            picker.show();
                            to_date.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Calendar cldr = Calendar.getInstance();
                                    int day = cldr.get(Calendar.DAY_OF_MONTH);
                                    int month = cldr.get(Calendar.MONTH);
                                    int year = cldr.get(Calendar.YEAR);
                                    // date picker dialog
                                    picker = new DatePickerDialog(project_main.this,
                                            new DatePickerDialog.OnDateSetListener() {
                                                @Override
                                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                                    to_date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                                }
                                            }, year, month, day);
                                    picker.show();
                                }
                            });
                        }
                    });
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // send data from the AlertDialog to the Activity
                                Log.e("task name",name.getText().toString());
                            Log.e("up pro when add task",String.valueOf(passed_project_id));
                            mydb.insertTask(passed_project_id,0,name.getText().toString(),from_date.getText().toString(),to_date.getText().toString(),duration.getText().toString(),cost.getText().toString());
                            Toast.makeText(project_main.this, "done",
                                    Toast.LENGTH_SHORT).show();
                            PlaceholderFragment.checklists.size();
                            PlaceholderFragment.checklists.add(new taskModel(String.valueOf(PlaceholderFragment.checklists.size()+1),String.valueOf(passed_project_id),"0",name.getText().toString(),from_date.getText().toString(),to_date.getText().toString(),duration.getText().toString(),cost.getText().toString()));
                            PlaceholderFragment.adapter.notifyDataSetChanged();
                        }
                            });
                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    // create and show the alert dialog
                    AlertDialog dialog = alert.create();
                    dialog.show();

                } else if (tabs.getSelectedTabPosition() == 1) {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(project_main.this);
                    alert.setTitle("Enter resource information");
                    final View customLayout = getLayoutInflater().inflate(R.layout.custom_layout_dialog, null);
                    alert.setView(customLayout);
                    final TextView poV = customLayout.findViewById(R.id.poV);
                    poV.setText("Resource name");
                    final EditText name = customLayout.findViewById(R.id.name2);
                    final TextView toV = customLayout.findViewById(R.id.toV);
                    final TextView foV = customLayout.findViewById(R.id.foV);
                    toV.setVisibility(View.GONE);
                    foV.setVisibility(View.GONE);
                    final EditText from_date = customLayout.findViewById(R.id.date3);
                    final EditText to_date = customLayout.findViewById(R.id.date4);
                    from_date.setVisibility(View.GONE);
                    to_date.setVisibility(View.GONE);
                    final TextView dur = customLayout.findViewById(R.id.duview);
                    dur.setText("Type");
                    final EditText duration = customLayout.findViewById(R.id.duration);
                    final TextView cos = customLayout.findViewById(R.id.costview);
                    cos.setVisibility(View.GONE);
                    final EditText cost = customLayout.findViewById(R.id.cost);
                    cost.setVisibility(View.GONE);
                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // send data from the AlertDialog to the Activity
                            Log.e("task name",name.getText().toString());
                            mydb.insertResource(passed_project_id,name.getText().toString(),duration.getText().toString());
                            PlaceholderFragment.checklists2.size();
                            PlaceholderFragment.checklists2.add(new resourceModel(String.valueOf(PlaceholderFragment.checklists2.size()+1),String.valueOf(passed_project_id),name.getText().toString(),duration.getText().toString()));
                            PlaceholderFragment.adapter.notifyDataSetChanged();
                            Toast.makeText(project_main.this, "done",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    // create and show the alert dialog
                    AlertDialog dialog = alert.create();
                    dialog.show();
                }
            }
        });
    }
}